=== Forum Redirect ===
Contributors: widgitlabs, evertiro
Donate link: https://evertiro.com/donate/
Tags: bbPress, BuddyPress, forum, forums, redirect
Requires at least: 3.0.1
Tested up to: 5.3.2
Stable tag: 1.0.0

Allows you to override the default behavior of bbPress forums, linking them to an external site.

== Description ==

Allows you to override the default behavior of bbPress forums, linking them to an external site. Forum Redirect requires no real configuration... it simply adds a metabox to the forum edit screen allowing you to specify an override URL.

== Installation ==

= From your WordPress dashboard =

1. Visit 'Plugins > Add New'
2. Search for 'Forum Redirect'
3. Activate Forum Redirect from your Plugins page

= From WordPress.org =

1. Download Forum Redirect
2. Upload the 'forum-redirect' folder to the '/wp-content/plugins' directory of your WordPress installation
3. Activate Forum Redirect from your Plugins page

== Frequently Asked Questions ==

None yet

== Changelog ==

= 1.0.0 =
* Initial release

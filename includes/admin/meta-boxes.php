<?php
/**
 * Meta boxes
 *
 * @package         ForumRedirect\Admin\MetaBoxes
 * @since           1.1.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Add metabox to forum and topic post types
 *
 * @since       1.1.0
 * @return      void
 */
function forum_redirect_add_redirect_metabox() {
	add_meta_box( 'forum-redirect', __( 'Forum Redirect', 'forum-redirect' ), 'forum_redirect_display_redirect_metabox', 'forum', 'side', 'default' );
	add_meta_box( 'forum-redirect', __( 'Topic Redirect', 'forum-redirect' ), 'forum_redirect_display_redirect_metabox', 'topic', 'side', 'default' );
}
add_action( 'add_meta_boxes', 'forum_redirect_add_redirect_metabox' );


/**
 * Display redirect metabox
 *
 * @since       1.1.0
 * @param       array $post The WP post object.
 * @return      void
 */
function forum_redirect_display_redirect_metabox( $post ) {
	$meta = get_post_custom( $post->ID );

	// Update old meta data.
	if ( isset( $meta['bbp-redirect'] ) && ! isset( $meta['forum-redirect'] ) ) {
		$meta['forum-redirect'] = $meta['bbp-redirect'];
	}

	$redirect = isset( $meta['forum-redirect'] ) ? esc_attr( $meta['forum-redirect'][0] ) : '';

	wp_nonce_field( basename( __FILE__ ), 'forum_redirect_metabox_nonce' );

	echo '<input type="text" name="forum-redirect" id="forum-redirect" value="' . esc_url( $redirect ) . '" style="width: 100%" />';
	// Translators: The URL to forward to.
	echo '<p class="howto">' . sprintf( esc_html__( 'Enter a URL to forward this %s to.', 'forum-redirect' ), esc_attr( $post->post_type ) ) . '</p>';
}


/**
 * Save redirect metabox
 *
 * @since       1.1.0
 * @param       int $post_id The ID of this post.
 * @global      object $post The post we are saving
 * @return      void
 */
function forum_redirect_save_redirect_metabox( $post_id ) {
	global $post;

	// Don't process if nonce can't be validated.
	if ( ! isset( $_POST['forum_redirect_metabox_nonce'] ) || ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['forum_redirect_metabox_nonce'] ) ), basename( __FILE__ ) ) ) {
		return;
	}

	// Don't process if this is an autosave.
	if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) || isset( $_REQUEST['bulk_edit'] ) ) {
		return;
	}

	// Don't process if this is a revision.
	if ( isset( $post->post_type ) && 'revision' === $post->post_type ) {
		return;
	}

	// Save new redirect, if set.
	if ( isset( $_POST['forum-redirect'] ) ) {
		update_post_meta( $post_id, 'forum-redirect', esc_url_raw( wp_unslash( $_POST['forum-redirect'] ) ) );
	} else {
		delete_post_meta( $post_id, 'forum-redirect' );
	}
}
add_action( 'save_post', 'forum_redirect_save_redirect_metabox' );
